import cv2
import os
import re
from tensorflow.keras.utils import to_categorical
import numpy as np


def read_dir_images(image_size, path, label):
    exp = re.compile(".+(\.jpg|\.bmp|\.png|\.jpeg)")

    images = []
    labels = []
    names = []
    for entry in os.scandir(path):
        if entry.is_file() and exp.match(entry.name):
            img = cv2.imread(entry.path)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = np.stack([img, img, img], axis=-1)
            img = cv2.resize(img, image_size)
            images.append(img)
            labels.append(label)
            names.append(entry.name)

    return images, labels, names


def read_dataset(image_size):
    images = []
    labels = []  # 0 is for negative and 1 is for positive label
    images_name = []

    for entry in os.scandir("dataset"):
        if entry.is_dir() and entry.name == "positives":
            img, lbl, names = read_dir_images(image_size, entry.path, 1)
            images += img
            labels += lbl
            images_name += names
        elif entry.is_dir() and entry.name == "negatives":
            img, lbl, names = read_dir_images(image_size, entry.path, 0)
            images += img
            labels += lbl
            images_name += names

    return np.array(images), np.array(labels).astype(np.float32), np.array(images_name)
