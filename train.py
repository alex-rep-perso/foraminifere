import re
import sys
import os
import cv2
import tensorflow as tf
import tensorflow.keras.layers as kl
from tensorflow.keras.models import Model
from tensorflow.keras.applications.efficientnet import preprocess_input
from focal_loss import BinaryFocalLoss
import argparse
import dataset
from sklearn.model_selection import train_test_split, StratifiedShuffleSplit, ParameterSampler
from sklearn.utils.class_weight import compute_sample_weight
import numpy as np
from sklearn.metrics import f1_score, precision_score, recall_score
import image_utils
import matplotlib.pyplot as plt
from scipy.stats import norm, randint, binom

LABELS_NAMES = {0: "negative", 1: "foraminifere"}

EN_MODEL_PARAMS = {
    "B0": [tf.keras.applications.efficientnet.EfficientNetB0, (224, 224, 3)],
    "B1": [tf.keras.applications.efficientnet.EfficientNetB1, (240, 240, 3)],
    "B2": [tf.keras.applications.efficientnet.EfficientNetB2, (260, 260, 3)],
    "B3": [tf.keras.applications.efficientnet.EfficientNetB3, (300, 300, 3)],
    "B4": [tf.keras.applications.efficientnet.EfficientNetB4, (380, 380, 3)],
    "B5": [tf.keras.applications.efficientnet.EfficientNetB5, (456, 456, 3)],
    "B6": [tf.keras.applications.efficientnet.EfficientNetB6, (528, 528, 3)],
    "B7": [tf.keras.applications.efficientnet.EfficientNetB7, (600, 600, 3)],
                   }


def create_parser():
    parser = argparse.ArgumentParser()

    parser.add_argument("--batch_size", type=int, default=32, help="Batch size for training")
    parser.add_argument("--train", action="store_true", default=False, help="launch a training session")
    parser.add_argument("--focal_loss", action="store_true", default=False, help="use focal loss")
    parser.add_argument("--eval", action="store_true", default=False, help="evaluate the current model")
    parser.add_argument("--test", type=str, help="Test a folder with images")
    parser.add_argument("--save_fig_eval", type=str, help="Saving figure when evaluating")
    parser.add_argument("--num_epochs", type=int, default=10, help="Number of epochs for training")
    parser.add_argument("--fine_tuning_blocks", nargs="*", default=[6, 7],
                        help="Starting and ending block to use for fine tuning, 1-7 are valid block number")
    parser.add_argument("--hyperparameter_search", type=int, help="Number of iteration for hyperparameter search")
    parser.add_argument("--num_classes", type=int, default=2, help="Number of classes for training")
    parser.add_argument("--lr", type=float, default=1e-3, help="Learning rate")
    parser.add_argument("--alpha", type=float, help="Alpha parameter for focal loss")
    parser.add_argument("--gamma", type=float, help="Gamma paramater for focal loss")
    parser.add_argument("--lr_fine_tune", type=float, help="Learning rate for the fine tuning phase")
    parser.add_argument("--model_type", type=str, required=True, help="Type of efficient net model", choices=EN_MODEL_PARAMS.keys())
    parser.add_argument("--save_path", type=str,
                        default="efficient_net_b4-7_aug_{epoch:02d}_{val_loss:.5f}.hdf5",
                        help="Save path for model weights")
    parser.add_argument("--load_model", type=str, help="Load path for model weights")

    return parser


def get_model(im_size, model_class):
    base_model = model_class(weights="imagenet", include_top=False,
                                                                   input_shape=im_size)

    x = base_model.output
    x = kl.GlobalAvgPool2D()(x)
    x = kl.Dropout(0.35)(x)
    pred = kl.Dense(1, activation='sigmoid')(x)

    model = Model(inputs=base_model.input, outputs=pred)

    # base_model.summary()

    return model, base_model


def get_callbacks(save_path):
    early_stopping = tf.keras.callbacks.EarlyStopping(monitor="val_loss", patience=10)
    checkpoint = tf.keras.callbacks.ModelCheckpoint(save_path, save_best_only=True, save_weights_only=True)
    reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor="val_loss", factor=0.5, patience=3, verbose=1)

    return [early_stopping, checkpoint, reduce_lr]


def data_generator(X, y, batch_size, sample_weights=None,
                   do_flip=True, do_transform=True, do_warp=True, validation=False,
                   im_size=(224, 224)):
    assert len(X) == len(y)

    indexes = np.arange(len(X))
    batch_x = []
    batch_y = []
    batch_w = []

    while True:

        if not validation:
            np.random.shuffle(indexes)

        for i in indexes:

            if not validation:
                params = image_utils.gen_warp_params(im_size[0], do_flip)

                img = image_utils.warp_by_params(params, X[i], can_warp=do_warp, can_flip=do_flip,
                                                 can_transform=do_transform,
                                                 border_replicate=True)

                batch_x.append(img)
            else:
                batch_x.append(X[i])

            batch_y.append(y[i])

            if not sample_weights is None:
                batch_w.append(sample_weights[i])

            if len(batch_x) >= batch_size:

                if not sample_weights is None:
                    yield np.stack(batch_x, axis=0), np.stack(batch_y, axis=0), np.array(batch_w)
                else:
                    yield np.stack(batch_x, axis=0), np.stack(batch_y, axis=0)

                batch_x = []
                batch_y = []
                batch_w = []


def show_image_grid(images, labels=None, predictions=None, names=None, raw_preds=None, title="Image grid",
                    save_fig=None):
    max_images_shown = 32

    images = images[:max_images_shown]
    if not labels is None:
        labels = labels[:max_images_shown]
    if not predictions is None:
        predictions = predictions[:max_images_shown]

    max_img_per_row = 8

    num_rows = len(images) // max_img_per_row
    if len(images) % max_img_per_row > 0:
        num_rows += 1

    fig, axs = plt.subplots(num_rows, max_img_per_row, figsize=(20, 14), constrained_layout=True)

    spec = fig.add_gridspec(ncols=max_img_per_row, nrows=num_rows)
    fig.suptitle(title)
    print(axs.shape)
    if len(axs.shape) == 1:
        axs = axs[np.newaxis, :]
    idx = 0
    for row in range(num_rows):
        for col in range(max_img_per_row):
            if idx >= len(images):
                # Removing axes when there is no images
                for r in range(row, num_rows):
                    for c in range(col, max_img_per_row):
                        axs[r][c].set_axis_off()
                break
            axs[row][col].imshow(images[idx])
            name = names[idx] if not names is None else idx
            if not labels is None:
                str_label = "Image {} \n Label {} \n Prediction {}".format(name, LABELS_NAMES[int(labels[idx])],
                                                                       LABELS_NAMES[int(predictions[
                                                                                            idx])]) if not predictions is None else "Image {} \n Label {}".format(
                name, labels[idx])
            else:
                str_label = "Image {} \n Prediction {}".format(name, LABELS_NAMES[int(predictions[idx])]) if not predictions is None else "Image {}".format(name)
            if not raw_preds is None:
                str_label = str_label + " \n Probs {}".format(str(raw_preds[idx]))

            axs[row][col].set_title(str_label)
            axs[row][col].set_axis_off()

            idx += 1


    plt.show()
    if save_fig:
        fig.savefig(save_fig)


class KfoldModel():

    def __init__(self, models):
        assert isinstance(models, list)

        self.models = models

    def predict(self, X):
        res = np.stack([m.predict(X) for m in self.models], axis=0)
        res = np.mean(res, axis=0)

        return res

    def eval(self, X):
        losses = [m.eval(X) for m in self.models]

        results = []
        num_models = len(self.models)
        for i in range(len(losses[0])):
            value = 0
            for lv in losses:
                value += lv[i] / num_models

            results.append(value)

        return results


DEFAULT_MODEL_PARAMS = {
    "lr_fine_tune": 5e-5,
    "blocks_fine_tuned": "7 6 5",
    "gamma": 2.0,
    "alpha": 0.25,
    "focal_loss": False
}


def train_model(X_train, X_val, y_train, y_val, model, base_model, args, model_name, im_size, params=None, verbose=1):
    if params is None:
        params = DEFAULT_MODEL_PARAMS
        if args.lr_fine_tune:
            params["lr_fine_tune"] = args.lr_fine_tune
        if args.alpha:
            params["alpha"] = args.alpha
        if args.gamma:
            params["gamma"] = args.gamma

        params["focal_loss"] = args.focal_loss

    if verbose > 0:
        print("training with parameters : {}".format(str(params)))
    # Determine fine tune blocks:
    try:
        fn_blocks = [int(m) for m in args.fine_tuning_blocks]
    except ValueError:
        raise ValueError("Error fine_tuning_blocks argument must all be integer")

    assert all([7 >= m >= 1 for m in fn_blocks]), "Error fine_tuning_blocks argument must all be between 1 and 7"

    fn_blocks = list(set(fn_blocks))  # remove duplicates
    fn_blocks.sort()
    fn_blocks = tuple(["block{}".format(b) for b in fn_blocks])
    print("fine tuning blocks : {}".format(fn_blocks))

    optimizer = tf.keras.optimizers.Adam(lr=args.lr)
    # Freezing bottom layers first
    print("number of layers in base model {}".format(len(base_model.layers)))
    for layer in base_model.layers:
        layer.trainable = False

    loss_fn = BinaryFocalLoss(gamma=params["gamma"], pos_weight=params["alpha"]) if params[
        "focal_loss"] else tf.keras.losses.BinaryCrossentropy()

    model.compile(optimizer=optimizer, loss=loss_fn, metrics="accuracy")

    sample_weights = None if params["focal_loss"] else compute_sample_weight("balanced", y_train)
    train_gen = data_generator(X_train, y_train, args.batch_size, sample_weights=sample_weights, im_size=im_size)
    num_steps_train = len(X_train) // args.batch_size + 1

    print("training top layers first")
    model.fit(X_train, y=y_train, batch_size=args.batch_size, epochs=10, validation_data=(X_val, y_val),
              sample_weight=sample_weights, verbose=verbose)

    print("Fine tuning the rest of the model")
    first_print = True
    for i, layer in enumerate(base_model.layers):
        if layer.name.startswith(fn_blocks):
            if first_print:
                print('Fine tune starting at layer {}'.format(i))
                first_print = False
            layer.trainable = True

    optimizer = tf.keras.optimizers.Adam(lr=params["lr_fine_tune"])
    model.compile(optimizer=optimizer, loss=loss_fn, metrics="accuracy")

    callbacks = get_callbacks(model_name)
    model.fit(train_gen, steps_per_epoch=num_steps_train, epochs=args.num_epochs,
              validation_data=(X_val, y_val), callbacks=callbacks, verbose=verbose)


def run_hyperparemeters_search(num_iter, X_train, X_val, y_train, y_val, im_size, args):
    param_dist = {
        "blocks_fine_tuned": randint(2, 8),
        "gamma": norm(loc=2, scale=0.8),
        "alpha": norm(loc=0.25, scale=0.1),
        "focal_loss": randint(0, 10),
        "lr_fine_tune": norm(loc=5e-5, scale=2e-5)
    }

    idx_iter = 0
    min_loss = 1000000
    min_iter = 0
    min_loss_focal = False
    max_accuracy = 0
    best_params = {}
    for params in ParameterSampler(param_dist, n_iter=num_iter):

        print("Starting iteration {} with parameters {}".format(idx_iter, params))

        args.fine_tuning_blocks = [str(8 - i) for i in range(1, params["blocks_fine_tuned"])]
        params["focal_loss"] = params["focal_loss"] <= 4

        if params["gamma"] <= 0:
            params["gamma"] = 0.1
        if params["alpha"] <= 0:
            params["alpha"] = 0.05
        if params["lr_fine_tune"] <= 0:
            params["lr_fine_tune"] = 1e-5

        model, base_model = get_model(im_size, EN_MODEL_PARAMS[args.model_type][0])
        model_name = "hyperparemeter_search_efficientnet_{}.hdf5".format(idx_iter)

        train_model(X_train, X_val, y_train, y_val, model, base_model, args, model_name, im_size, params=params,
                    verbose=0)
        model.load_weights(model_name)

        loss, accuracy = model.evaluate(X_val, y=y_val, batch_size=args.batch_size, verbose=0)

        print("Iteration {} with loss {} and accuracy {}".format(idx_iter, loss, accuracy * 100))
        if (loss < min_loss and params["focal_loss"] == min_loss_focal and not params["focal_loss"]) or (accuracy > max_accuracy and (params["focal_loss"] or params["focal_loss"] != min_loss_focal)):
            min_loss = loss
            min_iter = idx_iter
            max_accuracy = accuracy
            min_loss_focal = params["focal_loss"]
            best_params = params.copy()

        idx_iter += 1

    print("Best iteration : {} with loss {} - accuaracy {} and params {}".format(min_iter, min_loss, max_accuracy * 100, str(best_params)))


if __name__ == "__main__":

    parser = create_parser()

    args = parser.parse_args()

    im_size = EN_MODEL_PARAMS[args.model_type][1]

    if not args.test:
        images, labels, names = dataset.read_dataset(im_size[:2])
        X = preprocess_input(images)

        print(X.shape)
        print(labels.shape)

        sss = StratifiedShuffleSplit(n_splits=1, test_size=0.25, random_state=42)
        train_idx, val_idx = next(sss.split(X, labels))

        X_train = X[train_idx]
        X_val = X[val_idx]
        y_train = labels[train_idx]
        y_val = labels[val_idx]

    if args.hyperparameter_search:
        run_hyperparemeters_search(args.hyperparameter_search, X_train, X_val, y_train, y_val, im_size, args)
        sys.exit(0)

    model, base_model = get_model(im_size, EN_MODEL_PARAMS[args.model_type][0])

    if args.load_model:
        model.load_weights(args.load_model)

    if args.train:
        train_model(X_train, X_val, y_train, y_val, model, base_model, args, args.save_path, im_size)

    if args.eval:
        model.compile(optimizer="Adam", loss="binary_crossentropy", metrics="accuracy")

        loss, accuracy = model.evaluate(X_val, y=y_val, batch_size=args.batch_size)
        print("loss {} - accuracy {}".format(loss, accuracy * 100))

        results = model.predict(X_val, batch_size=args.batch_size)

        res_reduced = np.round(results).astype(np.int32).squeeze(axis=-1)
        labels = y_val.astype(np.int32)
        print(res_reduced)
        print(labels)

        print("Number of negative in labels {} - number of positives {}".format(len(np.nonzero(labels == 0)[0]),
                                                                                len(np.nonzero(labels == 1)[0])))
        print(
            "Number of negative in predictions {} - number of positives {}".format(len(np.nonzero(res_reduced == 0)[0]),
                                                                                   len(np.nonzero(res_reduced == 1)[
                                                                                           0])))

        score = f1_score(labels, res_reduced, labels=range(args.num_classes), average='micro')
        precision = precision_score(labels, res_reduced, labels=range(args.num_classes), average='micro')
        precision_macro = precision_score(labels, res_reduced, labels=range(args.num_classes), average='macro')

        recall = recall_score(labels, res_reduced, labels=range(args.num_classes), average='micro')

        print('f1 score: {:.3f}, precision: {:.3f} - macro {:.3f} - recall: {:.3f}'.format(score, precision,
                                                                                           precision_macro, recall))

        id_mislabel = np.nonzero(res_reduced != labels)[0]

        print("num mislabel {}".format(len(id_mislabel)))
        show_image_grid(X_val[id_mislabel], labels[id_mislabel], predictions=res_reduced[id_mislabel],
                        names=names[val_idx][id_mislabel], raw_preds=results[id_mislabel], title="Mispredictions",
                        save_fig=args.save_fig_eval)

        for idx, cls in enumerate(np.arange(args.num_classes)):
            score = f1_score(labels, res_reduced, labels=[idx], average='macro')
            precision = precision_score(labels, res_reduced, labels=[idx], average='macro')
            recall = recall_score(labels, res_reduced, labels=[idx], average='macro')
            print('For class {} - {} : f1 score: {:.3f}, precision: {:.3f}, recall: {:.3f}'.format(idx, cls, score,
                                                                                                   precision, recall))

    elif args.test:
        exp = re.compile(".+(\.jpg|\.bmp|\.png|\.jpeg)")

        images = []
        names = []
        for entry in os.scandir(args.test):
            if entry.is_file() and exp.match(entry.name):
                img = cv2.imread(entry.path)
                if img is None:
                    raise ValueError("Error cannot open image {}".format(entry.name))
                img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                img = np.stack([img, img, img], axis=-1)
                img = cv2.resize(img, im_size[:2])
                images.append(img)
                names.append(entry.name)

        X = preprocess_input(np.array(images))
        names = np.array(names)
        results = model.predict(X, batch_size=args.batch_size)
        res_reduced = np.round(results).astype(np.int32).squeeze(axis=-1)

        show_image_grid(X, None, predictions=res_reduced, names=names, raw_preds=results, title="Test")